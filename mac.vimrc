"set foldmethod=syntax
"set foldnestmax=10
"set nofoldenable
"set foldlevel=2
set wildmenu
set nowrap

highlight DiffAdd    cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffDelete cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
highlight DiffChange cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=red
" highlight DiffText   cterm=bold ctermfg=10 ctermbg=88 gui=none guifg=bg guibg=Red
highlight DiffText term=reverse ctermfg=15 ctermbg=9 guifg=White guibg=Red
" Convenience mappings ---------------------------------------------------- {{{
" Save
noremap s :w<cr>
" Toggle line numbers
nnoremap <leader>n :setlocal number!<cr>
" Wrap
nnoremap <leader>W :set wrap!<cr>

vnoremap u :<C-u>echo "ERROR : case change key is disabled !"<CR>
" Searching and movement -------------------------------------------------- {{{

" Use sane regexes.
nnoremap / /\v
vnoremap / /\v

set ignorecase
set smartcase
set incsearch
set showmatch
set hlsearch
set gdefault

set scrolloff=3
set sidescroll=1
set sidescrolloff=10

set virtualedit+=block

noremap <silent> <leader><space> :noh<cr>:call clearmatches()<cr>

" Keep search matches in the middle of the window.
nnoremap n nzzzv
nnoremap N Nzzzv

" " Same when jumping around
" nnoremap g; g;zz
" nnoremap g, g,zz
" nnoremap <c-o> <c-o>zz

" Easier to type, and I never use the default behavior.
noremap H ^
noremap L $
vnoremap L g_

" Heresy
inoremap <c-a> <esc>I
inoremap <c-e> <esc>A
cnoremap <c-a> <home>
cnoremap <c-e> <end>

noremap <leader>v <C-w>v

" }}}
" Visual Mode */# from Scrooloose {{{

function! s:VSetSearch()
  let temp = @@
  norm! gvy
  let @/ = '\V' . substitute(escape(@@, '\'), '\n', '\\n', 'g')
  let @@ = temp
endfunction

vnoremap * :<C-u>call <SID>VSetSearch()<CR>//<CR><c-o>
vnoremap # :<C-u>call <SID>VSetSearch()<CR>??<CR><c-o>

" }}}
" Folding ----------------------------------------------------------------- {{{

set foldlevelstart=0

" Space to toggle folds.
nnoremap <Space> za
vnoremap <Space> za
" set foldmethod=syntax
:au BufRead,BufNewFile *.c,*.h,*.y,*.l set foldmethod=syntax
set foldlevelstart=99

" }}}
" Clear undo history ------------------------------------------------------------ {{{
function! <SID>ForgetUndo()
	let old_undolevels = &undolevels
	set undolevels=-1
	exe "normal a \<BS>\<Esc>"
	let &undolevels = old_undolevels
	unlet old_undolevels
endfunction
command -nargs=0 ClearUndo call <SID>ForgetUndo()
" }}}
" Other settings ----------------------------------------------------------------- {{{
" Clean trailing whitespace
nnoremap <leader>ww mz:%s/\s\+$//<cr>:let @/=''<cr>`z

" }}}
"
" Window Resizing {{{
" right/up : bigger
" left/down : smaller
nnoremap <m-right> :vertical resize +3<cr>
nnoremap <m-left> :vertical resize -3<cr>
nnoremap <m-up> :resize +3<cr>
nnoremap <m-down> :resize -3<cr>
" }}}
"
" Commands to shorten 'cs find ...' things --------------------------------------- {{{
comm! -nargs=1 S :cs find s <args>
comm! -nargs=1 C :cs find c <args>
comm! -nargs=1 T :cs find t <args>
comm! -nargs=1 E :cs find e <args>
comm! -nargs=1 F :cs find f <args>
comm! -nargs=1 G :cs find g <args>

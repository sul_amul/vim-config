# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi
#bind '"^[[A":history-search-backward'
#bind '"^[[B":history-search-forward'
bind '"\C-p":history-search-backward'
bind '"\C-n":history-search-forward'
# User specific environment and startup programs
EDBAS=/home/amul/work/source/EDBAS
export EDBAS
cd $EDBAS
parse_git_branch1() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/' | tr -d -c 0-9
}
if [ -z "$RM" ]; then
	export RM=$(parse_git_branch1)
fi

if [ -z "$RM" ]; then
	export RM=5444
fi

#unalias sstart
#unalias sstop
#unalias srestart
#unalias sconfig
#unalias sinit
#unalias spsql

# ppas server start, stop & restart commands
alias sstart="/tmp/RM$RM/pg_inst/bin/pg_ctl -c -D /tmp/RM$RM/data start"
alias sstop="/tmp/RM$RM/pg_inst/bin/pg_ctl -D /tmp/RM$RM/data stop"
alias srestart="sstop; sstart"

# compliation options
alias sconfig="./configure --with-openssl --with-zlib --with-tcl --with-perl  --with-ossp-uuid --with-ldap --enable-nls --enable-cassert --enable-debug --enable-depend --with-pgport=$RM --prefix=/tmp/RM$RM/pg_inst CFLAGS='-g -O0 -Wno-error=unused-variable' "

alias sconfig_final="./configure --with-openssl --with-zlib --with-tcl --with-perl  --with-ossp-uuid --with-ldap --enable-nls --enable-cassert --enable-debug --enable-depend --with-pgport=$RM --prefix=/tmp/RM$RM/pg_inst"

alias sconfig2="./configure --with-openssl --with-zlib --with-tcl --with-perl  --with-ossp-uuid --with-ldap --enable-nls --enable-cassert --enable-debug --enable-depend --with-pgport=$RM --prefix=/tmp/RM$RM/pg_inst CFLAGS='-g -O0 -Wno-error=unused-variable -Wno-error=unused-but-set-variable' "
alias sconfig_oci="./configure --with-oci=/mnt/pgdata/u01/app/oracle/rdbms/public --with-openssl --with-zlib --with-tcl --with-perl  --with-ossp-uuid --with-ldap --enable-nls --enable-cassert --enable-debug --enable-depend --with-pgport=$RM --prefix=/tmp/RM$RM/pg_inst CFLAGS='-g -O0 -Wno-error=switch'"
# ppas server init & connect
alias sinit="sstop; rm -rf /tmp/RM$RM/data; /tmp/RM$RM/pg_inst/bin/initdb -D /tmp/RM$RM/data"
#alias psql="$dir/pg_inst/bin/psql"
alias spsql="/tmp/RM$RM/pg_inst/bin/psql -d edb"
alias ssetup="(sconfig && make install && sinit && sstart)"

# ppas server start, stop & restart commands
master_dir=/home/amul/Public/pg_inst/ppas-master
master_data=/home/amul/Public/pg_data/ppas-master
alias minstall="make distclean; ./configure --with-openssl --with-zlib --with-tcl --with-perl  --with-ossp-uuid --with-ldap --enable-nls --enable-cassert --enable-debug --enable-depend --with-pgport=5444 --prefix=$master_dir CFLAGS='-g -O0 -Wno-error=unused-variable'; rm -rf $master_dir; make install"
alias mstart="$master_dir/bin/pg_ctl -c -D $master_data start"
alias mstop="$master_dir/bin/pg_ctl -D $master_data stop"
alias minit="mstop; rm -rf $master_data; $master_dir/bin/initdb -D $master_data"
alias mpsql="$master_dir/bin/psql -d edb"

# ppas server start, stop & restart commands
ppas96_dir=/home/amul/Public/pg_inst/ppas96
ppas96_data=/home/amul/Public/pg_data/ppas96
alias ppas96_install="git checkout EDBAS9_6_STABLE; make maintainer-clean; ./configure --with-openssl --with-zlib --with-tcl --with-perl  --with-ossp-uuid --with-ldap --enable-nls --enable-cassert --enable-debug --enable-depend --with-pgport=5496 --prefix=$ppas96_dir CFLAGS='-g -O0 -Wno-error=unused-variable -Wno-error=unused-but-set-variable'; rm -rf $ppas96_dir; make install;"
alias ppas96_start="$ppas96_dir/bin/pg_ctl -c -D $ppas96_data start"
alias ppas96_stop="$ppas96_dir/bin/pg_ctl -D $ppas96_data stop"
alias ppas96_init="mstop; rm -rf $ppas96_data; $ppas96_dir/bin/initdb -D $ppas96_data"
alias ppas96_psql="$ppas96_dir/bin/psql -d edb"

ppas95_dir=/home/amul/Public/pg_inst/ppas95
ppas95_data=/home/amul/Public/pg_data/ppas95
alias ppas95_install="git checkout EDBAS9_5_STABLE; make maintainer-clean; ./configure --with-openssl --with-zlib --with-tcl --with-perl  --with-ossp-uuid --with-ldap --enable-nls --enable-cassert --enable-debug --enable-depend --with-pgport=5495 --prefix=$ppas95_dir CFLAGS='-g -O0 -Wno-error=unused-variable -Wno-error=unused-but-set-variable'; rm -rf $ppas95_dir; make install;"
alias ppas95_start="$ppas95_dir/bin/pg_ctl -c -D $ppas95_data start"
alias ppas95_stop="$ppas95_dir/bin/pg_ctl -D $ppas95_data stop"
alias ppas95_init="mstop; rm -rf $ppas95_data; $ppas95_dir/bin/initdb -D $ppas95_data"
alias ppas95_psql="$ppas95_dir/bin/psql -d edb"

ppas94_dir=/home/amul/Public/pg_inst/ppas94
ppas94_data=/home/amul/Public/pg_data/ppas94
alias ppas94_install="git checkout EDBAS9_4_STABLE; make maintainer-clean; ./configure --with-openssl --with-zlib --with-tcl --with-perl  --with-ossp-uuid --with-ldap --enable-nls --enable-cassert --enable-debug --enable-depend --with-pgport=5494 --prefix=$ppas94_dir CFLAGS='-g -O0 -Wno-error=unused-variable -Wno-error=unused-but-set-variable'; rm -rf $ppas94_dir; make install;"
alias ppas94_start="$ppas94_dir/bin/pg_ctl -c -D $ppas94_data start"
alias ppas94_stop="$ppas94_dir/bin/pg_ctl -D $ppas94_data stop"
alias ppas94_init="mstop; rm -rf $ppas94_data; $ppas94_dir/bin/initdb -D $ppas94_data"
alias ppas94_psql="$ppas94_dir/bin/psql -d edb"

ppas93_dir=/home/amul/Public/pg_inst/ppas93
ppas93_data=/home/amul/Public/pg_data/ppas93
alias ppas93_install="git checkout EDBAS9_3_STABLE; make maintainer-clean; ./configure --with-openssl --with-zlib --with-tcl --with-perl  --with-ossp-uuid --with-ldap --enable-nls --enable-cassert --enable-debug --enable-depend --with-pgport=5493 --prefix=$ppas93_dir CFLAGS='-g -O0 -Wno-error=unused-variable -Wno-error=unused-but-set-variable'; rm -rf $ppas93_dir; make install;"
alias ppas93_start="$ppas93_dir/bin/pg_ctl -c -D $ppas93_data start"
alias ppas93_stop="$ppas93_dir/bin/pg_ctl -D $ppas93_data stop"
alias ppas93_init="mstop; rm -rf $ppas93_data; $ppas93_dir/bin/initdb -D $ppas93_data"
alias ppas93_psql="$ppas93_dir/bin/psql -d edb"

ppas92_dir=/home/amul/Public/pg_inst/ppas92
ppas92_data=/home/amul/Public/pg_data/ppas92
alias ppas92_install="git checkout EDBAS9_2_STABLE; make maintainer-clean; ./configure --with-openssl --with-zlib --with-tcl --with-perl  --with-ossp-uuid --with-ldap --enable-nls --enable-cassert --enable-debug --enable-depend --with-pgport=5492 --prefix=$ppas92_dir CFLAGS='-g -O0 -Wno-error=unused-variable -Wno-error=unused-but-set-variable'; rm -rf $ppas92_dir; make install;"
alias ppas92_start="$ppas92_dir/bin/pg_ctl -c -D $ppas92_data start"
alias ppas92_stop="$ppas92_dir/bin/pg_ctl -D $ppas92_data stop"
alias ppas92_init="mstop; rm -rf $ppas92_data; $ppas92_dir/bin/initdb -D $ppas92_data"
alias ppas92_psql="$ppas92_dir/bin/psql -d edb"

# ppas server start, stop & restart commands
pg_master_dir=/home/amul/Public/pg_inst/pg-master
pg_master_data=/home/amul/Public/pg_data/pg-master
alias pginstall="pushd .; cd /home/amul/work/source/postgresql/; make maintainer-clean; ./configure --with-openssl --with-tcl --with-perl --with-python --enable-shared --with-distutils --with-zlib --enable-depend --enable-debug --enable-cassert --enable-tap-tests --prefix=$pg_master_dir CFLAGS='-g -O0'; rm -rf $pg_master_dir; make install; popd"
alias pgstart="$pg_master_dir/bin/pg_ctl -c -D $pg_master_data start"
alias pgstop="$pg_master_dir/bin/pg_ctl -D $pg_master_data stop"
alias pginit="pgstop; rm -rf $pg_master_data; $pg_master_dir/bin/initdb -D $pg_master_data"
alias pgpsql="$pg_master_dir/bin/psql -d postgres"
alias pgrestart="pgstop;pgstart"
alias make_check="pushd . && cd /tmp/regression_tests/ && make check && popd"

alias pgindent='../postgresql/src/tools/pgindent/pgindent'

PATH=$PATH:$HOME/.local/bin:$HOME/bin
PATH=/tmp/RM$RM/pg_inst/bin:$PATH
export EDITOR=vim
export PATH
export LC_ALL=en_US.UTF-8  
export LANG=en_US.UTF-8
